import axios from 'axios'
class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      favoritecolor: "red",
      payments: []
    };
  }
  componentDidMount() {
    axios.get('https://rest.toktok.mn/mobile/getPayment2').then((data) =>{
      this.state.payments = data.data
    })
  }
  // componentDidUpdate() {
  //   document.getElementById("mydiv").innerHTML =
  //   "The updated favorite is " + this.state.favoritecolor;
  // }
  render() {
    return (
      <div>
        {/* <h1>My Favorite Color is {this.state.favoritecolor}</h1> */}
        <div>{this.state.payments}</div>
        <ul> {this.state.payments.map(payment => {
          <li>{payment.name_mon}</li>
        })} </ul>
      </div>
    );
  }
}

export default Index;
